package texy.core;

/**
 * User entity
 */
public class User {

    /**
     * Known IP
     */
    private String ip;

    /**
     * Known listening port
     */
    private int port;

    /**
     * Visible name
     */
    private String nick;

    /**
     * Logon e-mail
     */
    private String email;

    /**
     * Last seen by server
     */
    private long timestamp;

    /**
     * Group
     */
    private Group group;

    /**
     * Default constructor
     */
    public User() {
        ip = "0.0.0.0";
        email = "buddy@texy.im";
        nick = "";
        group = null;
    }

    /**
     * Create user basing on incoming request
     * Used when message sent from unknown user
     *
     * @param response
     */
    public User(Packet response, Message message) {
        ip = response.getIp();
        port = response.getPort();
        email = message.getSenderEmail();
        nick = "";
        group = null;
    }

    /**
     * Get nick
     *
     * @return
     */
    public String getNick() {
        if (nick.isEmpty()) {
            return email;
        }

        return nick;
    }

    /**
     * Set nick
     *
     * @param nick
     */
    public void setNick(final String nick) {
        this.nick = nick;
    }

    /**
     * Get IP
     *
     * @return
     */
    public String getIp() {
        return ip;
    }

    /**
     * Set IP
     *
     * @param ip
     */
    public void setIp(final String ip) {
        this.ip = ip;
    }

    /**
     * Get e-mail
     *
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set e-mail
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get port
     *
     * @return
     */
    public int getPort() {
        return port;
    }

    /**
     * Set port
     *
     * @param port
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Last seen by server
     *
     * @return
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Get last seen by server
     *
     * @param timestamp
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}