package texy.core;

import java.util.ArrayList;

public class TaskManager {

    /**
     * Task worker running delay
     */
    public static final int RUN_DELAY = 100;

    /**
     * Application reference
     */
    private App app;

    /**
     * Asynchronous task list
     */
    private ArrayList<Task> tasks;

    /**
     * Task ID counter
     */
    private long taskId;

    /**
     * Threaded worker
     */
    private Worker worker;

    /**
     * Worker running flag
     */
    private Boolean running;

    /**
     * Constructor
     *
     * @param app
     */
    public TaskManager(App app) {
        this.app = app;

        worker = new Worker();
        running = false;
        tasks = new ArrayList<Task>();
        taskId = 0;
    }

    /**
     * Get maximum task ID
     *
     * @return
     */
    public long getTaskId() {
        return taskId;
    }

    /**
     * Send asynchronous task
     *
     * @param task
     */
    public void add(Task task) throws Error {
        Packet request = new Packet();
        request.setPort(app.getListener().getPort());

        task.setRequest(request);
        task.buildRequest();

        request = task.getRequest();
        if (request == null) {
            throw new Error("Task request is not specified");
        }

        task.setId(taskId++);

        tasks.add(task);
        app.getSender().addPacket(request);
    }

    /**
     * Receive asynchronous task
     */
    private void receive() throws Error {

        // Get packet from incoming queue
        Packet response = app.getListener().getPacket();

        // Nope, no response this time
        if (response == null) {
            return;
        }

        // Some response received, find relative task
        Task task = null;

        long id = response.getTaskId();
        if (id == Task.SERVER_ID || id == Task.CLIENT_ID) {

            // Create task by packet type
            String type = response.getType();
            String taskName = type.substring(0,1).toUpperCase() + type.substring(1);

            try {
                Class classObj = Class.forName("texy.logic." + taskName);
                task = (Task) classObj.newInstance();
            } catch (Exception e) {
                System.out.println("Class for servicing server (internal) task not found");
                System.out.println(e.getMessage());
                e.printStackTrace();
            }

        } else {
            // Find task by ID
            for (Task t : tasks) {
                if (t.getId() == id) {
                    task = t;
                    break;
                }
            }
        }

        if (task == null) {
            throw new Error("Invalid task ID");
        }

        task.setResponse(response);

        tasks.remove(task);

        // Do task post action
        task.serviceResponse();
    }

    /**
     * Set running
     *
     * @param flag
     */
    public void setRunning(Boolean flag) {
        running = flag;

        if (flag) {
            worker = new Worker();
            worker.start();
        } else {
            worker = null;
        }
    }

    /**
     * Check whether listener is running
     *
     * @return
     */
    public Boolean getRunning() {
        return running;
    }

    /**
     * Clear all tasks
     */
    public void clearTasks() {
        tasks.clear();
        taskId = 0;
    }

    /**
     * Threaded worker
     */
    class Worker extends Thread {

        /**
         * Main function
         */
        public void run() {
            System.out.println("Task manager is starting...");

            while (running) {
                try {
                    if (app.isNetworking()) {
                        receive();
                    }
                } catch (Exception e) {
                    System.out.println("Task manager error");
                    System.out.println(e.getMessage());
                }

                try {
                    Thread.sleep(RUN_DELAY);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            System.out.println("Task manager ended");
        }
    }
}
