package texy.core;

import java.util.ArrayList;

/**
 * Group entity
 */
public class Group {

    /**
     * Name
     */
    private String name;

    /**
     * User list
     */
    private ArrayList<User> users;

    /**
     * Constructor
     */
    public Group() {
        this.users = new ArrayList<User>();
    }

    /**
     * Get name
     *
     * @return Name
     */
    public String getName() {
        return name;
    }

    /**
     * Set name
     *
     * @param name Name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Get all users
     *
     * @return Users
     */
    public User[] getUsers() {
        return users.toArray(new User[users.size()]);
    }

    /**
     * Check whether user belongs to
     *
     * @param user User
     *
     * @return User
     */
    public boolean hasUser(User user) {
        return users.contains(user);
    }

    /**
     * Add user
     *
     * @param user User
     *
     * @return User
     */
    public boolean addUser(User user) {
        if (hasUser(user)) {
            return false;
        }

        users.add(user);

        return true;
    }

    /**
     * Remove user
     *
     * @param user User
     *
     * @return User
     */
    public boolean removeUser(User user) {
        if (!hasUser(user)) {
            return false;
        }

        users.remove(user);

        return true;
    }

    /**
     * Clear group users
     */
    public void removeAllUsers() {
        users.clear();
    }

    /**
     * Get user by index
     *
     * @param index
     * @return
     */
    public User getUser(int index) {
        return users.get(index);
    }
}
