package texy.core;

import org.json.simple.JSONObject;

/**
 * Exchanger packet representation
 */
public class Packet {

    /**
     * Incoming ip
     */
    private String ip;

    /**
     * Client listen port
     */
    private int port;

    /**
     * Type
     */
    private String type;

    /**
     * Current timestamp
     */
    private long timestamp;

    /**
     * User's email
     */
    private String email;

    /**
     * Content
     */
    private JSONObject content;

    /**
     * Task ID
     */
    private long taskId;

    /**
     * Constructor
     */
    public Packet() {
        ip = "0.0.0.0";
        port = 0;
        type = "default";
        timestamp = System.currentTimeMillis() / 1000;
        email = "buddy@texy.im";
        content = new JSONObject();
        taskId = Task.CLIENT_ID;
    }

    /**
     * Get ip
     *
     * @return
     */
    public String getIp() {
        return ip;
    }

    /**
     * Set ip
     *
     * @param ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * Set listen port
     *
     * @param port
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Get client listen port
     *
     * @return
     */
    public int getPort() {
        return port;
    }

    /**
     * Get type
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     * Set type
     *
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Get email
     *
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set email
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get timestamp
     *
     * @return
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Set timestamp
     *
     * @param timestamp
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Get content
     *
     * @return
     */
    public JSONObject getContent() {
        return content;
    }

    /**
     * Set content
     *
     * @param content
     */
    public void setContent(JSONObject content) {
        this.content = content;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }
}
