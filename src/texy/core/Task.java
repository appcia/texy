package texy.core;

abstract public class Task {

    /**
     * ID of task initialized by itself (client)
     */
    public final static int CLIENT_ID = -1;

    /**
     * ID of task initialized by server
     */
    public final static int SERVER_ID = -2;

    /**
     * Task ID
     */
    protected long id;

    /**
     * Request packet
     */
    protected Packet request;

    /**
     * Response packet
     */
    protected Packet response;

    /**
     * Application reference
     */
    protected App app;

    /**
     * Constructor
     */
    public Task() {
        app = App.instance();

        request = null;
        response = null;
    }

    /**
     * Get ID
     *
     * @return
     */
    public long getId() {
        return id;
    }

    /**
     * Set ID
     *
     * @param id
     */
    public void setId(long id) {
        this.id = id;

        if (request != null) {
            request.setTaskId(id);
        }
    }

    /**
     * Set request
     *
     * @param request
     */
    public void setRequest(Packet request) {
        this.request = request;
    }

    /**
     * Get request
     *
     * @return
     */
    public Packet getRequest() {
        return request;
    }

    /**
     * Set response
     *
     * @param response
     */
    public void setResponse(Packet response) {
        this.response = response;
    }

    /**
     * Get serviceResponse
     *
     * @return
     */
    public Packet getResponse() {
        return response;
    }

    /**
     * Action before sending task request
     *
     * @return
     */
    public abstract void buildRequest();

    /**
     * Action after receiving task response
     */
    public abstract void serviceResponse();
}