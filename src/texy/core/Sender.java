package texy.core;

import javax.swing.*;
import java.io.*;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Queue;

/**
 * User extension for communication
 */
public class Sender {

    /**
     * Worker run delay
     */
    public static final int RUN_DELAY = 100;

    /**
     * JSON parser
     */
    private Parser parser;

    /**
     * Outgoing packet queue
     */
    private Queue<Packet> packets;

    /**
     * Server hostname or IP
     */
    String host;

    /**
     * Server port number
     */
    int port;

    /**
     * Worker running flag
     */
    private Boolean running;

    /**
     * Threaded worker
     */
    private Worker worker;

    /**
     * Constructor
     */
    public Sender(Parser parser) {
        this.parser = parser;
        this.host = "0.0.0.0";
        this.port = 0;

        this.packets = new LinkedList<Packet>();
        this.worker = null;
        this.running = false;
    }

    /**
     * Set hostname or IP
     *
     * @param host
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Get server hostname
     *
     * @return
     */
    public String getHost() {
        return host;
    }

    /**
     * Set port
     *
     * @param port
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Get server port number
     *
     * @return
     */
    public int getPort() {
        return port;
    }

    /**
     * Set running flag
     *
     * @param flag
     */
    public void setRunning(Boolean flag) {
        this.running = flag;

        if (flag) {
            worker = new Worker();
            worker.start();
        } else {
            worker = null;
        }
    }

    /**
     * Check whether sender is running
     *
     * @return
     */
    public boolean getRunning() {
        return running;
    }

    /**
     * Add packet to queue
     *
     * @param packet
     */
    public void addPacket(Packet packet) {
        this.packets.add(packet);
    }

    /**
     * Remove all packets from queue
     */
    public void clearPackets() {
        packets.clear();
    }

    /**
     * Threaded worker
     */
    class Worker extends Thread {

        /**
         * Main function
         */
        public void run() {
            try {
                System.out.println("Sender is starting...");

                while (running) {
                    if (!packets.isEmpty()) {
                        Socket socket = new Socket(host, port);
                        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

                        Packet packet = packets.poll();
                        String response = parser.jsonifyPacket(packet);

                        out.write(response);

                        System.out.println(response);

                        out.close();
                        socket.close();
                    }

                    try {
                        Thread.sleep(RUN_DELAY);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                System.out.println("Sender ended");
            } catch (Exception e) {
                System.out.println("Sender error");
                System.out.println(e.getMessage());

                JOptionPane.showMessageDialog(null, "Could not send data to server");
            }
        }
    }
}
