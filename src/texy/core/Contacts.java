package texy.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Contact list manager
 */
public class Contacts {

    /**
     * Group list
     */
    private ArrayList<Group> groups;

    /**
     * User list
     */
    private ArrayList<User> users;

    /**
     * Default group
     */
    private Group defaultGroup;

    /**
     * Constructor
     */
    public Contacts() {
        groups = new ArrayList<Group>();
        users = new ArrayList<User>();

        defaultGroup = new Group();
        defaultGroup.setName("Buddy");
        groups.add(defaultGroup);
    }

    /**
     * Check whether group exist
     *
     * @param group Group
     *
     * @return Flag
     */
    public boolean hasGroup(Group group) {
        return groups.contains(group);
    }

    /**
     * Check whether user exist
     *
     * @param user User
     *
     * @return Flag
     */
    public boolean hasUser(User user) {
        return users.contains(user);
    }

    /**
     * Add user to default group
     *
     * @param user  User
     *
     * @return Flag
     */
    public boolean addUser(User user) {
        if (hasUser(user)) {
            return false;
        }

        Group group = user.getGroup();
        if (group == null) {
            group = defaultGroup;
        }

        user.setGroup(group);
        group.addUser(user);

        users.add(user);

        return true;
    }

    /**
     * Get groups
     *
     * @return Groups
     */
    public Group[] getGroups() {
        return groups.toArray(new Group[groups.size()]);
    }

    /**
     * Get users from all groups
     *
     * @return Users
     */
    public User[] getUsers() {
        return users.toArray(new User[users.size()]);
    }

    /**
     * Remove all contacts
     */
    public void removeAll() {
        for (Group group : groups) {
            group.removeAllUsers();
        }
        groups.clear();
        groups.add(defaultGroup);

        users.clear();
    }

    /**
     * Get group by name
     *
     * @param name
     * @return
     */
    public Group getGroup(String name) {
        for (Group group : groups) {
            if (group.getName() == name) {
                return group;
            }
        }

        return null;
    }

    /**
     * Get user by email
     *
     * @param email
     * @return
     */
    public User findUserByEmail(String email) {
        for (User user : users) {
            if (user.getEmail().equals(email)) {
                return user;
            }
        }

        return null;
    }
}
