package texy.core;

import org.json.simple.*;

public class Parser {

    /**
     * Check whether JSON data is correct
     *
     * @param json
     *
     * @return
     */
    public Boolean isJsonifyEmpty(String json) {
        return json.isEmpty();
    }

    /**
     * Create JSON string from pacjet object
     *
     * @param packet
     * @return
     */
    public String jsonifyPacket(Packet packet) {
        JSONObject obj = new JSONObject();

        obj.put("port", packet.getPort());
        obj.put("taskId", packet.getTaskId());
        obj.put("type", packet.getType());
        obj.put("email", packet.getEmail());
        obj.put("timestamp", packet.getTimestamp());
        obj.put("content", packet.getContent());

        return obj.toJSONString();
    }

    /**
     * Create packet from JSON data
     *
     * @param json
     *
     * @return
     */
    public Packet parsePacket(String json) {
        Packet packet = new Packet();

        JSONObject obj = (JSONObject) JSONValue.parse(json);

        packet.setPort(((Long) obj.get("port")).intValue());
        packet.setTaskId((Long) obj.get("taskId"));
        packet.setType((String) obj.get("type"));
        packet.setEmail((String) obj.get("email"));
        packet.setTimestamp(((Long) obj.get("timestamp")).intValue());
        packet.setContent((JSONObject) obj.get("content"));

        return packet;
    }
}
