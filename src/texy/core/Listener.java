package texy.core;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.Queue;

/**
 * User extension for communication
 */
public class Listener {

    /**
     * Worker run delay
     */
    public static final int RUN_DELAY = 100;

    /**
     * JSON parser
     */
    private Parser parser;

    /**
     * Incoming packet queue
     */
    private Queue<Packet> packets;

    /**
     * Server port number
     */
    int port;

    /**
     * Stopped flag
     */
    private Boolean running;

    /**
     * Threader worker
     */
    private Worker worker;

    /**
     * Server socket
     */
    private ServerSocket server;

    /**
     * Constructor
     */
    public Listener(Parser parser) {
        this.parser = parser;
        this.packets = new LinkedList<Packet>();
        this.worker = null;
        this.running = false;
    }

    /**
     * Set port number
     *
     * @param port
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Get server port number
     *
     * @return
     */
    public int getPort() {
        return port;
    }

    /**
     * Set running
     *
     * @param flag
     */
    public void setRunning(Boolean flag) {
        this.running = flag;

        if (flag) {
            worker = new Worker();
            worker.start();
        } else {
            if (server != null) {
                try {
                    server.close();
                } catch (IOException e) {
                }
                server = null;
            }

            worker = null;
        }
    }

    /**
     * Check whether listener is running
     *
     * @return
     */
    public Boolean getRunning() {
        return running;
    }

    /**
     * Get packet from listener
     * Could be null if queue is empty
     *
     * @return
     */
    public Packet getPacket() {
        return this.packets.poll();
    }

    /**
     * Remove all packets from queue
     */
    public void clearPackets() {
        packets.clear();
    }

    /**
     * Threaded worker
     */
    class Worker extends Thread {

        /**
         * Main function
         */
        public void run() {
            try {
                server = new ServerSocket(port);
                server.setReuseAddress(true);

                System.out.println("Listener is starting...");

                while (running) {
                    Socket socket = server.accept();
                    BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                    StringBuilder builder = new StringBuilder();
                    String line = "";

                    while ((line = in.readLine()) != null) {
                        builder.append(line);
                    }

                    in.close();
                    socket.close();

                    String request = builder.toString();

                    System.out.println(request);

                    Packet packet = parser.parsePacket(request);
                    packets.add(packet);

                    try {
                        Thread.sleep(RUN_DELAY);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (server != null) {
                    server.close();
                    server = null;
                }

                System.out.println("Listener ended");
            } catch (SocketException e) {
                System.out.println("Listener ended");
            } catch (Exception e) {
                System.out.println("Listener error");
                System.out.println(e.getMessage());

                JOptionPane.showMessageDialog(null, "Could not get data from server");
            }
        }
    }
}
