package texy.core;

import java.util.HashMap;
import java.util.Map;

public class App extends Thread {

    /**
     * Singleton creation
     */
    private static App instance = new App();

    /**
     * Common objects registry
     */
    private Map<String, Object> registry;

    /**
     * Task manager
     */
    private TaskManager taskManager;

    /**
     * Contacts
     */
    private Contacts contacts;

    /**
     * Currently logged user
     */
    private User loggedUser;

    /**
     * Currently visible group
     */
    private Group activeGroup;

    /**
     * Get application instance
     *
     * @return
     */
    public static App instance() {
        return instance;
    }

    /**
     * Constructor
     */
    private App() {
        networking = false;
        contacts = new Contacts();
        parser = new Parser();

        sender = new Sender(parser);
        listener = new Listener(parser);
        taskManager = new TaskManager(this);

        registry = new HashMap<String, Object>();
    }

    /**
     * Get object by name
     *
     * @param name
     *
     * @return
     */
    public Object get(String name) {
        return registry.get(name);
    }

    /**
     * Set object by name
     *
     * @param name
     * @param object
     */
    public void set(String name, Object object) {
        registry.put(name, object);
    }

    /**
     * Get task manager
     *
     * @return
     */
    public TaskManager getTaskManager() {
        return taskManager;
    }

    /**
     * Get contacts
     *
     * @return
     */
    public Contacts getContacts() {
        return contacts;
    }

    /**
     * Packet data JSON parser
     */
    private Parser parser;

    /**
     * Sender networking thread
     */
    private Sender sender;

    /**
     * Listener networking thread
     */
    private Listener listener;

    /**
     * Running flag
     */
    private Boolean networking;

    /**
     * Get sender thread
     *
     * @return
     */
    public Sender getSender() {
        return sender;
    }

    /**
     * Get listener thread
     *
     * @return
     */
    public Listener getListener() {
        return listener;
    }

    /**
     * Get JSON packet parser
     *
     * @return
     */
    public Parser getParser() {
        return this.parser;
    }

    /**
     * Get networking flag
     *
     * @return
     */
    public Boolean isNetworking() {
        return networking;
    }

    /**
     * Turn on / off networking
     *
     * @param flag
     */
    public void setNetworking(Boolean flag) {
        taskManager.setRunning(flag);
        sender.setRunning(flag);
        listener.setRunning(flag);

        if (!flag) {
            taskManager.clearTasks();
            sender.clearPackets();
            listener.clearPackets();
        }

        this.networking = flag;
    }

    /**
     * Check whether loggedUser is logged
     *
     * @return
     */
    public boolean isLogged() {
        return loggedUser != null;
    }

    /**
     * Get currently logged user
     *
     * @return
     */
    public User getLoggedUser() {
        return loggedUser;
    }

    /**
     * Set currently logged user
     *
     * @param loggedUser
     */
    public void setLoggedUser(User loggedUser) {
        this.loggedUser = loggedUser;
    }

    /**
     * Set active activeGroup
     *
     * @return
     */
    public Group getActiveGroup() {
        return activeGroup;
    }

    /**
     * Get acitve activeGroup
     *
     * @param activeGroup
     */
    public void setActiveGroup(Group activeGroup) {
        this.activeGroup = activeGroup;
    }
}