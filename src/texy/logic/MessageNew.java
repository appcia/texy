package texy.logic;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import texy.core.Message;
import texy.core.Task;
import texy.core.User;
import texy.gui.MainWindow;

import javax.swing.*;

public class MessageNew extends Task {

    private Message message;

    public MessageNew(Message message) {
        this.message = message;
    }

    public void buildRequest() {
        request.setEmail(app.getLoggedUser().getEmail());
        request.setType("messageNew");

        JSONObject msg = new JSONObject();
        msg.put("senderEmail", message.getSenderEmail());
        msg.put("recipientEmail", message.getRecipientEmail());
        msg.put("timestamp", message.getTimestamp());
        msg.put("content", message.getContent());

        JSONObject obj = new JSONObject();
        JSONArray arr = new JSONArray();

        arr.add(msg);
        obj.put("messages", arr);

        request.setContent(obj);
    }

    public void serviceResponse() {
        JSONObject content = response.getContent();

        int status = ((Long) content.get("status")).intValue();

        switch (status) {
            case 0:
                break;
            case -1:
                JOptionPane.showMessageDialog(null, "No messages to send");
                break;
            default:
                JOptionPane.showMessageDialog(null, "Cannot send messages. Unknown status code (" + status + ")");
                break;
        }
    }
}
