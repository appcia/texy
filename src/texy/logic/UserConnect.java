package texy.logic;

import org.json.simple.JSONObject;
import texy.core.Task;
import texy.core.User;
import texy.gui.MainWindow;

import javax.swing.*;

public class UserConnect extends Task {

    private String email;

    private String password;

    public UserConnect(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public void buildRequest() {
        request.setType("userConnect");

        JSONObject content = new JSONObject();
        content.put("email", email);
        content.put("password", password);

        request.setContent(content);
    }

    public void serviceResponse() {
        JSONObject content = response.getContent();

        int status = ((Long) content.get("status")).intValue();

        switch (status) {
            case 0:
                break;
            case -1:
                JOptionPane.showMessageDialog(null, "User '" + email + "' is not registered");
                break;
            case -2:
                JOptionPane.showMessageDialog(null, "Incorrect password");
                break;
            case 1:
                JOptionPane.showMessageDialog(null, "New instance of connection estabilished");
                break;
            default:
                JOptionPane.showMessageDialog(null, "Cannot login. Unknown status code (" + status + ")");
                break;
        }

        if (status >= 0) {
            User user = new User();
            user.setEmail(email);

            app.setLoggedUser(user);

            try {
                app.getTaskManager().add(new UserList());
            } catch (Exception e) {
                System.out.println("User list error");
                System.out.println(e.getMessage());
            }

            MainWindow mainWindow = (MainWindow) app.get("mainWindow");
            mainWindow.setLogged(true);
        }
    }
}
