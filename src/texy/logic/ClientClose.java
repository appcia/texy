package texy.logic;

import org.json.simple.JSONObject;
import texy.core.Listener;
import texy.core.Sender;
import texy.core.Task;
import texy.core.TaskManager;
import texy.gui.MainWindow;

import javax.swing.*;

public class ClientClose extends UserDisconnect {

    public ClientClose(String email) {
        super(email);
    }

    private int getDelay() {
        int delay = TaskManager.RUN_DELAY;

        if (Listener.RUN_DELAY > delay) {
            delay = Listener.RUN_DELAY;
        }

        if (Sender.RUN_DELAY > delay) {
            delay = Sender.RUN_DELAY;
        }

        return 2 * delay;
    }

    public void serviceResponse() {
        super.serviceResponse();

        app.setNetworking(false);

        try {
            Thread.sleep(getDelay());
            System.exit(0);
        } catch (Exception exception) {
            System.out.println("Exit error");
            System.out.println(exception.getMessage());
        }
    }
}
