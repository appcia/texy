package texy.logic;

import org.json.simple.JSONObject;
import texy.core.Task;
import texy.gui.MainWindow;

import javax.swing.*;

public class ServerClose extends Task {

    public void buildRequest() {}

    public void serviceResponse() {
        JSONObject content = response.getContent();

        int status = ((Long) content.get("status")).intValue();

        switch (status) {
            case 0:
                JOptionPane.showMessageDialog(null, "Server closed by administrator");
                break;
            default:
                JOptionPane.showMessageDialog(null, "Server closed with unknown status code (" + status + ")");
                break;
        }

        app.setLoggedUser(null);
        app.setActiveGroup(null);

        MainWindow mainWindow = (MainWindow) app.get("mainWindow");
        mainWindow.setConnected(status < 0);
        mainWindow.setLogged(status < 0);
    }
}
