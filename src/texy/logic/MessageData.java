package texy.logic;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import texy.core.Message;
import texy.core.Task;
import texy.core.User;
import texy.gui.ChatType;
import texy.gui.MainWindow;
import texy.gui.UserType;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

public class MessageData extends Task {

    public void buildRequest() {
    }

    public void serviceResponse() {
        JSONObject obj = response.getContent();
        JSONArray msgs = (JSONArray) obj.get("messages");

        ArrayList<ChatType> dialogs = new ArrayList<ChatType>();
        MainWindow mainWindow = (MainWindow) app.get("mainWindow");

        for (int i = 0; i < msgs.size(); i++) {
            JSONObject msg = (JSONObject) msgs.get(i);

            Message message = new Message();
            message.setContent((String) msg.get("content"));
            message.setSenderEmail((String) msg.get("senderEmail"));
            message.setRecipientEmail((String) msg.get("recipientEmail"));
            message.setTimestamp((Long) msg.get("timestamp"));

            User sender = app.getContacts().findUserByEmail(message.getSenderEmail());

            // Unknown sender
            if (sender == null) {
                sender = new User(response, message);
                app.getContacts().addUser(sender);
            }

            ChatType dialog = mainWindow.getChatDialog(sender);
            dialog.addMessage(message);

            if (!dialogs.contains(dialog)) {
                dialogs.add(dialog);
            }
        }

        for (ChatType dialog : dialogs) {
            dialog.open();
        }
    }
}
