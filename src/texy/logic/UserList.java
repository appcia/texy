package texy.logic;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import texy.core.Task;
import texy.core.User;
import texy.gui.MainWindow;

public class UserList extends Task {

    public void buildRequest() {
        request.setEmail(app.getLoggedUser().getEmail());
        request.setType("userList");
    }

    public void serviceResponse() {
        JSONObject content = response.getContent();
        JSONArray arr = (JSONArray) content.get("users");

        app.getContacts().removeAll();

        for (int i = 0; i < arr.size(); i++) {
            JSONObject obj = (JSONObject) arr.get(i);

            User user = new User();
            user.setIp((String) obj.get("ip"));
            user.setPort(((Long) obj.get("port")).intValue());
            user.setTimestamp((Long) obj.get("timestamp"));
            user.setEmail(((String) obj.get("email")));
            user.setNick(((String) obj.get("nick")));

            if (user.getNick().isEmpty()) {
                user.setNick(user.getEmail());
            }

            app.getContacts().addUser(user);
        }

        MainWindow mainWindow = (MainWindow) app.get("mainWindow");
        mainWindow.updateGroupCombo();
        mainWindow.updateContactList();
    }
}
