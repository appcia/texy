package texy.logic;

import org.json.simple.JSONObject;
import texy.core.Task;
import texy.gui.MainWindow;

import javax.swing.*;

public class UserDisconnect extends Task {

    private String email;

    public UserDisconnect(String email) {
        this.email = email;
    }

    public void buildRequest() {
        request.setEmail(app.getLoggedUser().getEmail());
        request.setType("userDisconnect");

        JSONObject content = new JSONObject();
        content.put("email", email);

        request.setContent(content);
    }

    public void serviceResponse() {
        JSONObject content = response.getContent();

        int status = ((Long) content.get("status")).intValue();

        switch (status) {
            case 0:
                break;
            case -1:
                JOptionPane.showMessageDialog(null, "User '" + email + "' is not registered");
                break;
            case -2:
                JOptionPane.showMessageDialog(null, "User '" + email + "' is not connected");
                break;
            default:
                JOptionPane.showMessageDialog(null, "Cannot logout. Unknown status code (" + status + ")");
                break;
        }

        app.setLoggedUser(null);

        MainWindow mainWindow = (MainWindow) app.get("mainWindow");
        mainWindow.setLogged(status < 0);
    }
}
