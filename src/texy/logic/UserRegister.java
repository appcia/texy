package texy.logic;

import org.json.simple.JSONObject;
import texy.core.Task;

import javax.swing.*;

public class UserRegister extends Task {

    private String email;

    private String password;

    private String nick;

    public UserRegister(String email, String password, String nick) {
        this.email = email;
        this.password = password;
        this.nick = nick;
    }

    public void buildRequest() {
        request.setType("userRegister");

        JSONObject content = new JSONObject();
        content.put("email", email);
        content.put("password", password);
        content.put("nick", nick);

        request.setContent(content);
    }

    public void serviceResponse() {
        JSONObject content = response.getContent();

        int status = ((Long) content.get("status")).intValue();

        switch (status) {
            case -1:
                JOptionPane.showMessageDialog(null, "Incorrect e-mail '" + email + "'");
                break;
            case -2:
                JOptionPane.showMessageDialog(null, "Incorrect password");
                break;
            case -3:
                JOptionPane.showMessageDialog(null, "E-mail '" + email + "' is already used");
                break;
            case -4:
                JOptionPane.showMessageDialog(null, "Invalid nick");
                break;
            case 0:
                JOptionPane.showMessageDialog(null, "User '" + email + "' registered successfully");
                break;
            default:
                JOptionPane.showMessageDialog(null, "Cannot register. Unknown status code (" + status + ")");
                break;
        }
    }
}
