package texy.gui;

import texy.core.User;

import javax.swing.*;
import java.awt.event.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserType extends JDialog {
    private JPanel contentPane;
    private JButton closeButton;
    private JTextField nameField;
    private JTextField ipField;
    private JLabel ipLabel;
    private JLabel nameLabel;
    private JTextField portField;
    private JLabel portLabel;
    private JTextField timestampField;
    private JLabel timestampLabel;

    public UserType() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(closeButton);
        pack();

        closeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        // addPacket your code here
        dispose();
    }

    private void onCancel() {
        // addPacket your code here if necessary
        dispose();
    }

    public void setData(User user) {
        nameField.setText(user.getNick());
        ipField.setText(user.getIp());
        portField.setText(String.valueOf(user.getPort()));
        timestampField.setText(new SimpleDateFormat("MM/dd/yyyy, h:mm a").format(new Date(user.getTimestamp() * 1000)));
    }

    public void getData(User user) {
        user.setNick(nameField.getText());
    }
}
