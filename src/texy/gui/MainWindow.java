package texy.gui;

import texy.core.*;
import texy.core.Error;
import texy.logic.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.event.*;
import java.util.ArrayList;

public class MainWindow {

    public static final int CLOSE_TIMEOUT = 3000;

    private static final App app = App.instance();
    private static JFrame frame;

    private JTabbedPane tabs;
    private JPanel panel1;
    private JButton connectButton;
    private JButton infoButton;
    private JButton syncButton;
    private JButton clearButton;
    private JComboBox groupCombo;
    private JList contactList;
    private JTextField serverIpField;
    private JLabel serverIpLabel;
    private JPanel content;
    private JTextField userEmailField;
    private JPasswordField userPasswordField;
    private JTextField serverPortField;
    private JLabel serverPortLabel;
    private JTextField clientPortField;
    private JLabel clientPortLabel;
    private JPanel settingsTab;
    private JPanel contactsTab;
    private JTextField userNickField;
    private JPanel accountTab;
    private JLabel userNickLabel;
    private JLabel userPasswordLabel;
    private JLabel userEmailLabel;
    private JButton actionButton;
    private JRadioButton loginRadio;
    private JRadioButton registerRadio;
    private JButton logoutButton;

    private ArrayList<ChatType> chatTypes;

    public MainWindow() {
        final MainWindow window = this;
        chatTypes = new ArrayList<ChatType>();

        infoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int index = contactList.getSelectedIndex();
                if (index >= 0) {
                    final UserType dialog = new UserType();
                    dialog.setLocationRelativeTo(null);

                    final User user = app.getActiveGroup().getUser(index);
                    dialog.setData(user);
                    dialog.addWindowListener(new WindowListener() {
                        @Override
                        public void windowOpened(WindowEvent e) {
                        }

                        @Override
                        public void windowClosing(WindowEvent e) {
                        }

                        @Override
                        public void windowClosed(WindowEvent e) {
                            dialog.getData(user);
                            window.updateContactList();
                        }

                        @Override
                        public void windowIconified(WindowEvent e) {
                        }

                        @Override
                        public void windowDeiconified(WindowEvent e) {
                        }

                        @Override
                        public void windowActivated(WindowEvent e) {
                        }

                        @Override
                        public void windowDeactivated(WindowEvent e) {
                        }
                    });

                    dialog.setVisible(true);
                }
            }
        });

        contactList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);

                JList list = (JList) e.getSource();
                if (e.getClickCount() == 2) {
                    int index = list.locationToIndex(e.getPoint());
                    if (index >= 0) {
                        Object o = list.getModel().getElementAt(index);

                        User user = app.getActiveGroup().getUser(index);
                        openChatDialog(user);
                    }
                }
            }
        });

        DocumentListener connectListener = new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                verify();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                verify();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                verify();
            }

            public void verify() {
                boolean proper = !serverIpField.getText().isEmpty() && !serverPortField.getText().isEmpty() && !clientPortField.getText().isEmpty();
                connectButton.setEnabled(!app.isLogged() && proper);
            }
        };

        serverIpField.getDocument().addDocumentListener(connectListener);
        serverPortField.getDocument().addDocumentListener(connectListener);
        clientPortField.getDocument().addDocumentListener(connectListener);

        connectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (!app.isNetworking()) {
                        app.getSender().setHost(serverIpField.getText());
                        app.getSender().setPort(Integer.valueOf(serverPortField.getText()));
                        app.getListener().setPort(Integer.valueOf(clientPortField.getText()));

                        app.setNetworking(true);
                        setConnected(true);
                    } else {
                        app.setNetworking(false);
                        setConnected(false);
                    }

                    userEmailField.requestFocus();

                } catch (Exception exception) {
                    app.setNetworking(false);
                    setConnected(false);

                    JOptionPane.showMessageDialog(null, exception.getMessage());
                    exception.printStackTrace();
                }
            }
        });
        syncButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    app.getTaskManager().add(new UserList());
                } catch (Exception exception) {
                    JOptionPane.showMessageDialog(null, exception.getMessage());
                }
            }
        });
        actionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (loginRadio.isSelected()) {
                        if (!app.isLogged()) {
                            app.getTaskManager().add(new UserConnect(userEmailField.getText(), String.valueOf(userPasswordField.getPassword())));
                        } else {
                            app.getTaskManager().add(new UserDisconnect(app.getLoggedUser().getEmail()));
                        }
                    } else if (registerRadio.isSelected()) {
                        app.getTaskManager().add(new UserRegister(userEmailField.getText(), String.valueOf(userPasswordField.getPassword()), userNickField.getText()));
                    }
                } catch (Exception exception) {
                    JOptionPane.showMessageDialog(null, exception.getMessage());
                }
            }
        });

        ChangeListener actionListener = new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (loginRadio.isSelected()) {
                    userNickField.setVisible(false);
                    userNickLabel.setVisible(false);
                    actionButton.setText(!app.isLogged() ? "Login" : "Logout");
                } else if (registerRadio.isSelected()) {
                    userNickField.setVisible(true);
                    userNickLabel.setVisible(true);
                    actionButton.setText("Register");
                }
            }
        };

        loginRadio.addChangeListener(actionListener);
        registerRadio.addChangeListener(actionListener);
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DefaultListModel model = (DefaultListModel) contactList.getModel();
                model.clear();
            }
        });
    }

    /**
     * Application main function
     *
     * @param args
     */
    public static void main(String[] args) {

        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
        } catch (Exception e) {
            System.out.println("Look and feel error");
            System.out.println(e.getMessage());
        }

        MainWindow window = new MainWindow();
        app.set("mainWindow", window);

        frame = new JFrame("Texy IM");
        frame.setContentPane(window.getContent());
        frame.pack();

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                if (app.isLogged()) {
                    try {
                        app.getTaskManager().add(new ClientClose(app.getLoggedUser().getEmail()));
                        try {
                            Thread.sleep(CLOSE_TIMEOUT);
                            System.exit(0);
                        } catch (Exception exception) {
                            System.out.println("Exit error");
                            System.out.println(exception.getMessage());
                        }
                    } catch (Error error) {
                        JOptionPane.showMessageDialog(null, error.getMessage());
                    }
                }
            }
        });

        frame.setVisible(true);
    }

    public JPanel getContent() {
        return content;
    }

    public void setConnected(boolean flag) {
        tabs.setSelectedIndex(!flag ? 0 : 1);
        tabs.setEnabledAt(1, flag);
        connectButton.setText(!flag ? "Connect" : "Disconnect");

        serverIpField.setEnabled(!flag);
        serverPortField.setEnabled(!flag);
        clientPortField.setEnabled(!flag);
    }

    public void updateGroupCombo() {
        DefaultComboBoxModel model = (DefaultComboBoxModel) groupCombo.getModel();
        model.removeAllElements();

        Group groups[] = app.getContacts().getGroups();
        if (groups.length > 0) {
            for (Group group : groups) {
                model.addElement(group.getName());
            }

            app.setActiveGroup(groups[0]);
            groupCombo.setSelectedIndex(0);
        }
    }

    public void updateContactList() {
        Group activeGroup = app.getContacts().getGroup((String) groupCombo.getSelectedItem());

        if (activeGroup == null) {
            return;
        }

        DefaultListModel model = (DefaultListModel) contactList.getModel();
        model.removeAllElements();

        for (User user : activeGroup.getUsers()) {
            model.addElement(user.getNick());
        }
    }

    public void setLogged(boolean flag) {
        tabs.setSelectedIndex(!flag ? 1 : 2);
        tabs.setEnabledAt(2, flag);
        actionButton.setText(!flag ? "Login" : "Logout");

        userEmailField.setEnabled(!flag);
        userPasswordField.setEnabled(!flag);
        userNickField.setEnabled(!flag);

        registerRadio.setEnabled(!flag);

        ((DefaultListModel) contactList.getModel()).clear();
    }

    public void openChatDialog(User user) {
        ChatType dialog = getChatDialog(user);
        dialog.open();
    }

    public ChatType getChatDialog(User user) {
        ChatType dialog = null;
        for (ChatType d : chatTypes) {
            if (d.getRecipient().equals(user)) {
                dialog = d;
                break;
            }
        }

        if (dialog == null) {
            dialog = new ChatType(user);
            chatTypes.add(dialog);
        }

        return dialog;
    }

    public void closeChatDialog(ChatType dialog) {
        chatTypes.remove(dialog);
        dialog.dispose();
    }
}
