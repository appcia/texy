package texy.gui;

import texy.core.*;
import texy.core.Error;
import texy.logic.MessageNew;

import javax.swing.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ChatType extends JDialog {
    private static final App app = App.instance();

    private JPanel contentPane;
    private JButton sendButton;
    private JTextField recipientField;
    private JList messageList;
    private JLabel chatLabel;
    private JLabel userLabel;
    private JLabel messageLabel;
    private JTextArea messageText;
    private JButton buttonCancel;

    User recipient;

    public ChatType(User r) {
        this.recipient = r;

        setContentPane(contentPane);
        setModal(false);
        getRootPane().setDefaultButton(sendButton);
        pack();

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Message message = new Message();
                    message.setSenderEmail(app.getLoggedUser().getEmail());
                    message.setRecipientEmail(recipient.getEmail());
                    message.setContent(messageText.getText());

                    app.getTaskManager().add(new MessageNew(message));
                    addMessage(message);
                } catch (Error ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
        });
    }

    private void onCancel() {
        MainWindow mainWindow = (MainWindow) app.get("mainWindow");
        mainWindow.closeChatDialog(this);
    }

    public User getRecipient() {
        return recipient;
    }

    /**
     * Initialize window after opening
     */
    public void init() {
        recipientField.setText(recipient.getNick());
        messageText.requestFocus();
    }

    public void open() {
        if (!isVisible()) {
            setLocationRelativeTo(null);
            pack();
            init();
        }

        setVisible(true);
    }

    /**
     * Add new message to list
     *
     * @param message
     */
    public void addMessage(Message message) {
        String str = new SimpleDateFormat("MM/dd/yyyy, h:mm a").format(new Date(message.getTimestamp() * 1000));
        str += " | " + message.getSenderEmail();
        str += " | " + message.getContent();

        DefaultListModel model = (DefaultListModel) messageList.getModel();
        model.addElement(str);

        messageList.setModel(model);
        messageText.setText("");
        messageText.requestFocus();

        pack();
    }
}
